#!/bin/bash
#By Ruat Loic 19/09/2024
# Acronyms Dictionary imported from : https://github.com/cloudsecurelab/security-acronyms.git
RED='\033[0;31m'
NC='\033[0m'
acro=""
dictionaryPath="/home/shiito/scripts/homescript/cyber_dictionary.txt"

function searchAcro {
	acroCapital=${acro^^}
	acroAnswer= cat $dictionaryPath | grep "$acroCapital"
}

echo "Bienvenue dans le glossaire personnel de Ruat Loïc"
echo "Veuillez rentrez l'acronyme qui vous questionne :"
read acro

if [ -z $acro ]
then 
	echo -e "${RED}Failed${NC}: Aucun acronyme renseigner"
	echo -e "Veuillez renseigner un acronyme"
else
	result=$(searchAcro)
	echo "$result"
fi




